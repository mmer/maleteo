<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class FormDemo extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $ciudades)
    {
        $builder->add('name');
        $builder->add('email', EmailType::class);
        $builder->add(
            'ciudades',
            ChoiceType::class, [
                'choices' => [
                    'Madrid' => 'madrid',
                    'Barcelona' => 'barcelona',
                    'Alicante' => 'alicante',
                    'Sevilla' => 'sevilla',
                ]
            ]
        );
        $builder->add('privacy', CheckboxType::class, [
            'label'    => "Aceptas la <a href='#'>política de privacidad </a>",
            'label_html' => true,
            'required' => true,
        ]);
        $builder->add('enviar', SubmitType::class);
    }
}
