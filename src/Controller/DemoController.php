<?php

namespace App\Controller;

use App\Form\FormDemo;
use App\Entity\Demo;
use App\Form\FormEdit;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DemoController extends AbstractController
{    
    /**
     * @Route ("/sendForm", name="sendForm")
     */
    public function validForm (EntityManagerInterface $doctrine, Request $request)
    {
        $formDemo = $this->createForm(FormDemo::class);
        $formDemo->handleRequest($request);

        // si viene de un submit, procesa los datos, si no, lo pintas en la home
        if ($formDemo->isSubmitted() && $formDemo->isValid()) {
            $datos = $formDemo->getData();
            $ciudad = $datos['ciudades'];
            $nombre = $datos['name'];

            $newDemo = new Demo ();
            $newDemo->setNombre($datos['name']);
            $newDemo->setEmail($datos['email']);
            $newDemo->setCiudad($ciudad);
   
            $doctrine->persist($newDemo);
            $doctrine->flush($newDemo);

            $this->addFlash('success', "¡Hola, $nombre! Hemos registrado tu solicitud y te enviaremos la demo lo antes posible.");

            return $this->redirectToRoute('home');

        } else { //aquí tengo que llevar a una ruta que diga que es error o algo así
            return $this->render(
                'base.html.twig',
                [
                    'formDemo' => $formDemo->createView(),
                ]
            );
        }
    }

    /**
     * @Route("/admin/demoList", name="demos")
     */
    public function getDemos(EntityManagerInterface $doctrine)  // mostrar la lista de demos solicitadas
    {
        $rep = $doctrine->getRepository(Demo::class);
        $demos = $rep->findAll();

        return $this->render('demoList.html.twig', ['newDemo' => $demos]);
    }

    /**
     * @Route("/admin/delete/{id}", name="delete")
     */
    public function deleteDemo(EntityManagerInterface $doctrine, $id)  // eliminar un registro de demo concreto
    {   
        $rep = $doctrine->getRepository(Demo::class);
        $demoToDelete = $rep->find($id);
        $doctrine->remove($demoToDelete);
        $doctrine->flush();

        return $this->redirectToRoute('demos');
    }

    /**
     * @Route("/admin/edit/{id}", name="edit")
     */
    public function editDemo(EntityManagerInterface $doctrine, $id)  // editar una solicitud de demo concreta. te lleva a una página nueva
    {
        $rep = $doctrine->getRepository(Demo::class);
        $demoToEdit = $rep->find($id);

        $formDemo = $this->createForm(FormEdit::class);

        return $this->render('editDemo.html.twig', 
        [
            'form' => $formDemo->createView(),
            'demo' => $demoToEdit,
        ]);
    }

    /**
     * @Route("/admin/updateDemo/{id}", name="updateDemo")
     */
    public function updateName(Request $request, $id)  // esta funcion actualiza la base de datos con la nueva demo
    {
        $form = $this->createForm(FormEdit::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $datos = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $demo = $entityManager->getRepository(Demo::class)->find($id);

            $demo->setNombre($datos['name']);
            $demo->setEmail($datos['email']);
            $demo->setCiudad($datos['ciudad']);

            $entityManager->flush();

            return $this->redirectToRoute('demos');

        } else { //aquí debería llevar a una ruta que diga que es error o algo así
            return $this->redirectToRoute('demos');
        }

    }
}
