<?php
namespace App\Controller;

use App\Entity\User;
use App\Form\FormSignUp;
use App\Security\LoginFormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class SignUpController extends AbstractController
{   
    /**
     * @Route ("/signup", name="app_signup")
     */
    public function newUser()
    {
        $formSignUp = $this->createForm(FormSignUp::class);

        return $this->render(
            'signup.html.twig',
            [
                'formNewUser' => $formSignUp->createView(),
            ]
        );
    }

    /**
     * @Route ("/checkUser", name="checkNewUser")
     */
    public function checkNewUser(EntityManagerInterface $doctrine, Request $request, UserPasswordEncoderInterface $encoder,
    GuardAuthenticatorHandler $guard, LoginFormAuthenticator $formAuthenticator)
    {
        $formSignUp = $this->createForm(FormSignUp::class);
        $formSignUp->handleRequest($request);

        if ($formSignUp->isSubmitted() && $formSignUp->isValid()) {
            $datos = $formSignUp->getData();

            $rep = $doctrine->getRepository(User::class);
            $users = $rep->findAll();

            $emailOK = true;
            // vamos a comprobar que el email no esté duplicado
            foreach ($users as $user){
                $email = $user->getUsername();
                if($email === $datos['email']){
                    $this->addFlash('success', "Por favor, prueba a registrate con otro email, este ya está siendo usado por otro usuario.");
                    $emailOK = false;
                    return $this->redirectToRoute('app_signup');
                    break;
                }
            }

            if($datos['password'] === $datos['password2'] && $emailOK){
                $password = $datos['password'];

                $newUser = new User ();
                $newUser->setName($datos['name']);
                $newUser->setEmail($datos['email']);
                $newUser->setPassword($encoder->encodePassword($newUser, $password)); // esto es para encriptar la contraseña
   
                $doctrine->persist($newUser);
                $doctrine->flush($newUser);

                // esta línea es para que cuando se registre, también inicie sesión
                return $guard->authenticateUserAndHandleSuccess($newUser, $request, $formAuthenticator, 'main');
            } else {
                $this->addFlash('success', "La contraseña es incorrecta.");
                return $this->redirectToRoute('app_signup');
            }

            return $this->redirectToRoute('home');

        } else { //aquí tengo que llevar a una ruta que diga que es error o algo así
            return $this->render(
                'signUp.html.twig',
                [
                    'formNewUser' => $formSignUp->createView(),
                ]
            );
        }
    }
}