<?php
namespace App\Controller;

use App\Entity\Opinion;
use App\Form\FormOpinion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OpinionController extends AbstractController
{
    /**
     * @Route ("/newOpinion", name="opinion")
     */
    public function insertOpinion(EntityManagerInterface $doctrine, Request $request)
    {
        $formOpinion = $this->createForm(FormOpinion::class);
        $formOpinion->handleRequest($request);

        if ($formOpinion->isSubmitted() && $formOpinion->isValid()) {
            $datos = $formOpinion->getData();

            $newOpinion = new Opinion ();
            $newOpinion->setAutor($datos['name']);
            $newOpinion->setComentario($datos['comment']);
            $newOpinion->setCiudad($datos['ciudad']);
   
            $doctrine->persist($newOpinion);
            $doctrine->flush($newOpinion);

            return $this->redirectToRoute('home');

        } else { //aquí tengo que llevar a una ruta que diga que es error o algo así
            return $this->render(
                'opinion.html.twig',
                [
                    'formOpinion' => $formOpinion->createView(),
                ]
            );
        }
    }

    /**
     * @Route("/admin/opinionList", name="opinions")
     */
    public function getOpinions(EntityManagerInterface $doctrine)  // mostrar la lista de opiniones
    {
        $rep = $doctrine->getRepository(Opinion::class);
        $opinions = $rep->findAll();

        return $this->render('opinionList.html.twig', ['opinions' => $opinions]);
    }

    /**
     * @Route("/admin/deleteOpinion/{id}", name="deleteOpinion")
     */
    public function deleteOpinion(EntityManagerInterface $doctrine, $id)  // eliminar un registro de opinion concreto
    {   
        $rep = $doctrine->getRepository(Opinion::class);
        $opinionToDelete = $rep->find($id);
        $doctrine->remove($opinionToDelete);
        $doctrine->flush();

        return $this->redirectToRoute('opinions');
    }

    /**
     * @Route("/admin/editOpinion/{id}", name="editOpinion")
     */
    public function editOpinion(EntityManagerInterface $doctrine, $id)  // editar una opinion concreta. te lleva a una página nueva
    {
        $rep = $doctrine->getRepository(Opinion::class);
        $opinionToEdit = $rep->find($id);

        $formOpinion = $this->createForm(FormOpinion::class);

        return $this->render('editOpinion.html.twig', 
        [
            'form' => $formOpinion->createView(),
            'opinion' => $opinionToEdit,
        ]);
    }

    /**
     * @Route("/admin/updateOpinion/{id}", name="updateOpinion")
     */
    public function updateName(Request $request, $id)  // actualiza la base de datos
    {
        $form = $this->createForm(FormOpinion::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $datos = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $opinion = $entityManager->getRepository(Opinion::class)->find($id);

            $opinion->setAutor($datos['name']);
            $opinion->setComentario($datos['comentario']);
            $opinion->setCiudad($datos['ciudad']);

            $entityManager->flush();

            return $this->redirectToRoute('opinions');

        } else { //aquí debería llevar a una ruta que diga que es error o algo así
            return $this->redirectToRoute('opinions');
        }

    }
}