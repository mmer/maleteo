<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\FormDemo;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Opinion;

class DefaultController extends AbstractController
{
    /**
     * @Route("/maleteo", name="home")
     */
    public function home(EntityManagerInterface $doctrine)
    {   
        $formDemo = $this->createForm(FormDemo::class);

        $rep = $doctrine->getRepository(Opinion::class);
        $opinions = $rep->findAll();
        shuffle($opinions);

        return $this->render(
            'base.html.twig',
            [
                'formDemo' => $formDemo->createView(),
                'opinions' => $opinions,
            ]);
    }
}